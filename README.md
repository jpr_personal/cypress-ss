## Install

To see the tests you need to install project and run cypress. Prerequesite is to have node and npm init

1. npm install
2. npx cypress open --browser chrome
3. click on customMade.spec.js

## Where is the code

1. https://bitbucket.org/jpr_personal/cypress-ss/src/develop/cypress/integration/customMade.spec.js contains the test
2. https://bitbucket.org/jpr_personal/cypress-ss/src/develop/cypress/fixtures/coat.json contains the test data
3. https://bitbucket.org/jpr_personal/cypress-ss/src/develop/cypress/support/commonHelpers.js contains the reusable functions