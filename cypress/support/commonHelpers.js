export function selectAttributeByCode(data, attribute) {
    cy.get('.cmtm-container > .hydrated')
        .shadow()
        .find(`[data-code=${attribute}]`)
        .click();
    cy.get('.cmtm-container > .hydrated')
        .shadow()
        .find('div.card-wrapper div.text')
        .contains(data[attribute])
        .click();
    applyStep();
}

export function selectCardByText(text) {
    cy.get('.cmtm-container > .hydrated')
        .shadow()
        .get('div.card-wrapper div.text')
        .contains(text)
        .click();
}

export function nextStep() {
    cy.get('.cmtm-container > .hydrated')
        .shadow()
        .find('.navigation-content [data-action="next"]')
        .click();
}

export function nextSection() {
    cy.get('.cmtm-container > .hydrated')
        .shadow()
        .find('[data-action="next"]')
        .click();
    cy.wait('@next');
}

export function addToCart() {
    cy.get('.cmtm-container > .hydrated')
        .shadow()
        .find('[data-action="next"]')
        .contains('Add')
        .click();
    cy.wait('@articleAdded');
}

export function applyStep() {
    cy.get('.cmtm-container > .hydrated')
        .shadow()
        .find('[data-action="apply"]')
        .click();
}

export function openAttributeByText(text) {
    cy.get('.cmtm-container > .hydrated')
        .shadow()
        .find('cmtm-definition-button')
        .contains(text)
        .click();
}

export function addInitials(data, initial) {
    openAttributeByText(initial);
    selectCardByText(data.initialsPosition);
    nextStep();
    cy.get('.cmtm-container > .hydrated')
        .shadow()
        .find('input[placeholder="Initials"]')
        .should('not.be.disabled')
        .type(data.initials, { force: true });
    nextStep();
    selectCardByText(data.initialsColor);
    nextStep();
    selectCardByText(data.initialsText);
    applyStep();
}

export function selectSize(data) {
    cy.get('.cmtm-container > .hydrated')
        .shadow()
        .find('cmtm-definition-button')
        .contains('Size')
        .click();
    cy.get('.cmtm-container > .hydrated')
        .shadow()
        .find(`ss-button[aria-label="${data.size}"]`)
        .click();
    applyStep();
}

export function closeCountryPopup() {
    cy.get('body').then(($body) => {
        if (
            $body.find(
                '.cookies__popup--country > .btn-close > .cookies__close-icon'
            ).length > 0
        ) {
            cy.get(
                '.cookies__popup--country > .btn-close > .cookies__close-icon'
            ).click();
        }
    });
}

export function getCustomMade(article) {
    cy.visit('/en-nl/journal/custom-made.html');
    cy.contains(`Custom Made ${article}`, { matchCase: false })
        .parents()
        .then((parents) => {
            cy.get(parents[1]).within(() => {
                cy.get('button').click({ scrollBehavior: 'center' });
            });
        });
    cy.get('.cmtm-container > .hydrated')
        .shadow()
        .get('div.card-wrapper')
        .should('be.visible');
    closeCountryPopup();
}

export function assertAttributeByCode(data, attribute) {
    if (attribute !== 'buttons') {
        cy.get('.cmtm-container > .hydrated')
            .shadow()
            .find(`[data-code=${attribute}]`)
            .should('contain', data[attribute]);
    } else {
        cy.get('.cmtm-container > .hydrated')
            .shadow()
            .find(`[data-code=${attribute}]`)
            .should('contain', data.buttonsName);
    }
}

export function assertAttributeByText(data, label, attribute) {
    cy.get('.cmtm-container > .hydrated')
        .shadow()
        .get('cmtm-definition-button div.name')
        .contains(label)
        .parent()
        .should('contain', data[attribute]);
}

export {};
