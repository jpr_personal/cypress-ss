import {
    selectAttributeByCode,
    addInitials,
    selectCardByText,
    nextSection,
    selectSize,
    addToCart,
    getCustomMade,
    assertAttributeByCode,
    assertAttributeByText
} from '../support/commonHelpers';

const article = 'coat';

describe('Using the Custom Made', () => {
    beforeEach(() => {
        // disable private cookies popup
        cy.setCookie('cookie-message', '1');
        // prepare data set
        cy.fixture(article).then((dataArticle) => {
            cy.wrap(dataArticle).as('dataArticle');
        });
        // prepare intercept for next actions
        cy.intercept('POST', 'GetRenderSchema').as('next');
        cy.intercept('POST', 'Cart-AddCustomMadeProducts').as('articleAdded');
    });
    it(`User completes the happy path for ${article} flow`, () => {
        // Get the CM for desired article
        getCustomMade(article);
        cy.get('@dataArticle').then((customizedValues) => {
            // Select Fabric
            selectCardByText(customizedValues.fabric);
            // Go Next
            nextSection();
            // Select Style
            selectAttributeByCode(customizedValues.style, 'fit');
            selectAttributeByCode(customizedValues.style, 'length');
            selectAttributeByCode(customizedValues.style, 'lining');
            selectAttributeByCode(customizedValues.style, 'buttons');
            addInitials(customizedValues.style, 'Add Initial');
            // Go Next
            nextSection();
            // Select Size
            selectSize(customizedValues);
            // Go Next
            nextSection();
            // Assert customized values
            assertAttributeByCode(customizedValues, 'fabric');
            assertAttributeByCode(customizedValues.style, 'fit');
            assertAttributeByCode(customizedValues.style, 'length');
            assertAttributeByCode(customizedValues.style, 'lining');
            assertAttributeByCode(customizedValues.style, 'buttons');
            assertAttributeByText(customizedValues.style, 'Initial', 'initialsPosition');
            assertAttributeByText(customizedValues.style, 'Initial', 'initials');
            assertAttributeByText(customizedValues.style, 'Initial', 'initialsColor');
            assertAttributeByText(customizedValues, 'Size', 'size');
            // Go to Cart
            addToCart();
            // Assert customized article
            cy.get('a.product-card__title-link').should(
                'contain',
                `Custom Made ${article.charAt(0).toUpperCase() + article.slice(1)}`
            );
        });
    });
});
